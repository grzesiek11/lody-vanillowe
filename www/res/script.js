const hash = window.location.hash.slice(1);
const hashElement = document.getElementById(hash);

if (hash && hashElement) {
    hashElement.classList.add("highlight");
}