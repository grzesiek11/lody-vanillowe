<?php
$pageDir = 'pages';
$resourceDir = 'res';
$dataDir = 'data';

$headerPage = 'header.html';
$footerPage = 'footer.html';
$error404page = 'error/404.html';
$error401page = 'error/401.html';
$mainPage = 'main.html';

$pageArg = 'p';
$title = 'Lody Vanillowe';
?>